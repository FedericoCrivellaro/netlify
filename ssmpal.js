const path        = require('path');
let reqPath       = path.join(__dirname, '../');
const tiny = require("tiny-json-http");

const Router          = require('koa-router'),
      utilities       = require(path.join(reqPath,'/modules/utilities'));

const client_id =  "d0f940b443c979e7c554";
const client_secret = "f8e77b9d4e57e946a3a383cfc80048e1a9caeb4a";
const authUrl = "https://github.com/login/oauth/authorize?client_id=d0f940b443c979e7c554&scope=repo,user";
const tokenUrl = "https://github.com/login/oauth/access_token";

const router       = new Router({ prefix: '/ssmpal' });


router.get('/', (ctx) => {
    ctx.body = `<a href="${authUrl}">Login with Github</a>`
});

router.get("/auth", (ctx) => {
  try{
      console.log("authUrl",authUrl);
      ctx.redirect(authUrl);
      ctx.body = 'Redirecting to git';
  }catch(e){
      console.log(e);
      ctx.body = 'Error';

  }
});


router.get('/success', (ctx) => {
  ctx.body('');
})

router.get("/callback", async (ctx) => {

  
    const data = {
      code: ctx.query.code,
      client_id,
      client_secret
    };
    
    try {
      const { body } = await tiny.post({ 
        url: tokenUrl, 
        data, 
        headers: {
          "Accept": "application/json" 
        } 
      });



      const postMsgContent = {
        token: body.access_token,
        provider: "github"
      };
      
      // This is what talks to the NetlifyCMS page. Using window.postMessage we give it the
      // token details in a format it's expecting
      const script = `
      <script>
      (function() {
        function recieveMessage(e) {
          console.log("recieveMessage %o", e);          
          window.opener.postMessage('authorization:github:success:${JSON.stringify(postMsgContent)}',e.origin);}
          window.addEventListener("message", recieveMessage, false);
          // Start handshare with parent
          console.log("Sending message: %o","github")
          window.opener.postMessage("authorizing:github","*");
        })()
      </script>`;
      ctx.body = script;
      
    } catch (err) {
      // If we hit an error we'll handle that here
      console.log("ERROR",err);
    }
  });

  
module.exports = router;